"""
Jump to stored flow position. Use dropdown menu to switch to stored position
KEY FEATURES:
* Delete single bookmark or all of them
* All bookmarks are alphabetically sorted.
* Add a bookmark from Jump UI with plus button. Press Refresh and new bookmarkk will appear in a list
* To jump the same tool once again just select it again in a list.
v 2.5 update:
* Move selected tools next to bookmark position (enable checkbox 'move selected to bookmark')
* Jump back and forth between previous and current bookmarks
v 2.6:
* Add tool ID next to Bookmark name
* fix move to Bookmark bug
* cach error on JumpBack function
v 2.8:
* Sorting bookmarks by name and tool type
* Button to remove orphaned bookmarks
* prevent multiple windows to launch
* attempt to find tool by ID, if bookmark was not found because tool was renamed 

KNOWN ISSUES:
* depending on complexity if the comp, the nodes in a flow
may temporarily disappear after bookmark jump. As a workaround to this issue
added 0.1 sec delay before jump to the tool.
* the script just finds a tool in a flow and makes it active. It does not center it in the flow.
Possible workaround here:
-- jump to the tool, click on the flow, press CTRL/CMD+F and then hit ENTER
-- right click the flow with node selected, Scale -> Scale to Fit

Alexey Bogomolov mail@abogomolov.com
Requests and issues: https://gitlab.com/WeSuckLess/Reactor/tree/movalex/Atoms/com.AlexBogomolov.Bookmarker
Donations: https://paypal.me/aabogomolov
STU topic and discussion, feature requests and updates: https://www.steakunderwater.com/wesuckless/viewtopic.php?f=33&t=2858

MIT License: https://mit-license.org/
"""
# legacy python reporting compatibility
from __future__ import print_function
import time


comp = fu.GetCurrentComp()
ui = fusion.UIManager
flow = comp.CurrentFrame.FlowView

regs_dictionary = {
    "Merge": "Mrg",
    "Merge3D": "3Mg",
    "SurfaceAlembicMesh": "ABC",
    "AlphaDivide": "ADv",
    "AlphaMultiply": "AMl",
    "LightAmbient": "3AL",
    "SSAO": "SSAO",
    "Anaglyph": "Ana",
    "Fuse.AnimationEaseModifier": "AEM",
    "AutoDomain": "ADoD",
    "AutoGain": "AG",
    "Background": "BG",
    "Bender3D": "3Bn",
    "Fuse.BezierTaper": "bztp",
    "Dimension.BicubicTest": "BCT",
    "BitmapMask": "Bmp",
    "MtlBlinn": "3Bl",
    "Fuse.Blip": "Blip",
    "Blur": "Blur",
    "BrightnessContrast": "BC",
    "BSplineMask": "BSp",
    "BumpMap": "3Bu",
    "Dimension.CalcOcclusions": "CO",
    "Camera3D": "3Cm",
    "CameraShake": "CShk",
    "Dimension.CameraTracker": "CTra",
    "TexCatcher": "3Ca",
    "ChangeDepth": "CD",
    "MtlChanBool": "3Bol",
    "ChannelBoolean": "Bol",
    "ChromaKeyer": "CKy",
    "Fuse.JD_ChromaticZoom": "crz",
    "CineonLog": "Log",
    "CleanPLate": "CPl",
    "ColorCorrector": "CC",
    "ColorCurves": "CCv",
    "ColorGain": "Clr",
    "CustomColorMatrix": "CMx",
    "ColorSpace": "CS",
    "Combiner": "Com",
    "Fuse.Conditional": "Cndl",
    "Fuse.Convolve": "Cnv",
    "MtlCookTorrance": "3CT",
    "CoordSpace": "CdS",
    "CoordTransform3D": "3CX",
    "Dimension.CopyAux": "CpA",
    "Fuse.FLS_CopyMetadata": "Meta",
    "CornerPositioner": "CPn",
    "CreateBumpMap": "CBu",
    "Crop": "Crp",
    "Fuse.Cryptomatte": "Cryptomatte",
    "Cube3D": "3Cb",
    "CubeMap": "3Cu",
    "CustomFilter": "CFlt",
    "Custom": "CT",
    "CustomVertex3D": "3CV",
    "DaySky": "DS",
    "Dimension.DecomposeOp": "De",
    "Defocus": "Dfo",
    "DeltaKeyer": "DK",
    "Dent": "Dnt",
    "DepthBlur": "DBl",
    "DifferenceKeyer": "DfK",
    "DirectionalBlur": "DrBl",
    "LightDirectional": "3DL",
    "Dimension.Disparity": "Dis",
    "Dimension.DisparityToZ": "D2Z",
    "Displace": "Dsp",
    "Displace3D": "3Di",
    "Dissolve": "DX",
    "Drip": "Drip",
    "Fuse.Duplicate": "Dup",
    "Duplicate3D": "3Dp",
    "Fuse.eLinBC": "lBC",
    "EllipseMask": "Elp",
    "ErodeDilate": "ErDl",
    "Fuse.ExternalMatteSaver": "EMS",
    "FalloffOperator": "3Fa",
    "FastNoiseTexture3D": "3FN",
    "FastNoise": "FN",
    "ExporterFBX": "FBX",
    "SurfaceFBXMesh": "FBX",
    "Dimension.FeatherMaskOp": "FeatherMask",
    "Fields": "Flds",
    "FileLUT": "FLUT",
    "FilmGrain": "FGr",
    "Filter": "Fltr",
    "Dimension.FixOpticalFlow": "FOF",
    "Fuse.FlexyRig": "Lin",
    "Fog": "Fog",
    "Fog3D": "3Fog",
    "Fuse.FractalNoise": "Ftns",
    "Fuse.FrameAverage": "Avg",
    "Fuse.FUI_Circles": "FCT",
    "Fuse.FUI_Grids": "FGT",
    "Dimension.FwdBack": "FwdBack",
    "GamutConvert": "Gmt",
    "Dimension.GlobalAlign": "GA",
    "Glow": "Glo",
    "TexGradient": "3Gd",
    "Grain": "Grn",
    "GridWarp": "Grd",
    "Dimension.GuidedFilterOp": "GF",
    "Highlight": "HiL",
    "HotSpot": "Hot",
    "HueCurves": "HCv",
    "ImagePlane3D": "3Im",
    "KeyStretcher": "KfS",
    "LatLongPatcher": "LLP",
    "LensDistort": "Lens",
    "Letterbox": "Lbx",
    "Fuse.LifeSaver": "LSV",
    "LightTrim": "LT",
    "Fuse.Linger": "Lgr",
    "Loader": "LD",
    "Locator3D": "3Lo",
    "LumaKeyer": "LKy",
    "Fuse.LUTCubeAnalyzer": "LCA",
    "Fuse.LUTCubeApply": "LCP",
    "Fuse.LUTCubeCreator": "LCC",
    "Mandel": "Man",
    "PaintMask": "PnM",
    "MtlMerge3D": "3MM",
    "MtlSwitch3D": "3MSw",
    "MatteControl": "Mat",
    "MediaIn": "MI",
    "MediaOut": "MO",
    "Dimension.Median3x3": "M3",
    "Dimension.NewEye": "NE",
    "Fuse.Normalize": "fNrl",
    "Note": "Nte",
    "Fuse.Notepad": "Not",
    "Fuse.NumberOperator": "fNOp",
    "OCIOCDLTransform": "OCD",
    "OCIOColorSpace": "OCC",
    "OCIOFileTransform": "OCF",
    "Dimension.OpticalFlow": "OF",
    "Override3D": "3Ov",
    "Paint": "Pnt",
    "PanoMap": "PaM",
    "pAvoid": "pAv",
    "pBounce": "pBn",
    "pChangeStyle": "pCS",
    "pCustom": "pCu",
    "pCustomForce": "pCF",
    "pDirectionalForce": "pDF",
    "pEmitter": "pEm",
    "PerspectivePositioner": "PPn",
    "pFlock": "pFl",
    "pFollow": "pFo",
    "pFriction": "pFr",
    "pGradientForce": "pGF",
    "MtlPhong": "3Ph",
    "pImageEmitter": "pIE",
    "Fuse.PixelSort": "PixS",
    "pKill": "pKl",
    "Dimension.PlanarTracker": "PTra",
    "Dimension.PlanarTransform": "PXf",
    "Plasma": "Plas",
    "pMerge": "pMg",
    "LightPoint": "3PL",
    "PointCloud3D": "3PC",
    "PolylineMask": "Ply",
    "Fuse.PositionHelper": "PHlp",
    "pPointForce": "pPF",
    "pRender": "pRn",
    "Photron.Primatte4": "Pri",
    "Profiler": "Pro",
    "LightProjector": "3Pj",
    "PropagateDepth": "PD",
    "PseudoColor": "PsCl",
    "pSpawn": "pSp",
    "pTangentForce": "pTF",
    "pTurbulence": "pTr",
    "pVortex": "pVt",
    "RangesMask": "Rng",
    "RankFilter": "RFlt",
    "Fuse.OCLRays": "ClR",
    "RectangleMask": "Rct",
    "MtlReflect": "3RR",
    "RemoveNoise": "RN",
    "Renderer3D": "3Rn",
    "Dimension.RepairFrame": "Rep",
    "ReplaceMaterial3D": "3Rpl",
    "ReplaceNormals3D": "3RpN",
    "Replicate3D": "3Rep",
    "BetterResize": "Rsz",
    "Fuse.Retimer": "rt",
    "Ribbon3D": "3Ri",
    "Fuse.RollingShutter": "Rol",
    "RunCommand": "Run",
    "Saver": "SV",
    "Scale": "Scl",
    "Dimension.Seclow": "Sec",
    "SetCanvasColor": "SCv",
    "SetDomain": "DoD",
    "Fuse.SetMetaData": "SMeta",
    "Fuse.SetMetaDataTC": "TCMeta",
    "Shader": "Shd",
    "Shadow": "Sh",
    "Shape3D": "3Sh",
    "Sharpen": "Shrp",
    "Fuse.ShowMetadata": "smd",
    "Dimension.SmoothMotion": "SM",
    "SoftGlow": "SGlo",
    "SoftClip": "3SC",
    "SphereMap": "3SpM",
    "SphericalCamera3D": "3SC",
    "Splitter": "Spl",
    "LightSpot": "3SL",
    "Dimension.StereoAlign": "SA",
    "MtlStereoMix3D": "3SMM",
    "Switch": "Sw",
    "Fuse.Switch": "Sw?",
    "Fuse.SwitchElse": "fSE",
    "Switch3D": "3Sw",
    "Text3D": "3Txt",
    "TextPlus": "Txt+",
    "Texture": "Txr",
    "Texture2DOperator": "3Tx",
    "TextureTransformOperator": "3TT",
    "Fuse.TEXTWRAPPING": "TwR",
    "Fuse.TimeMachine": "TMn",
    "Fuse.TimeMachinePoint": "TMn",
    "Fuse.TimeMachineImage": "TM",
    "Fuse.TimeMachine3D": "TM3D",
    "Fuse.TimeMachineMaterial": "TM3M",
    "TimeSpeed": "TSpd",
    "TimeStretcher": "TSt",
    "Fuse.Time3D": "T3D",
    "TrackerModifier": "Trk",
    "Tracker": "Tra",
    "Trails": "Trls",
    "Transform": "Xf",
    "Transform3D": "3Xf",
    "Triangulate3D": "3Tri",
    "Fuse.True_IK": "IK",
    "TV": "TV",
    "Dimension.Tween": "Tw",
    "TweenOld": "TwO",
    "UltraKeyer": "UKy",
    "Underlay": "Und",
    "UnsharpMask": "USM",
    "UVMap": "3UV",
    "VariBlur": "VBl",
    "Distort": "Dst",
    "VectorMotionBlur": "VBl",
    "VolumeFog": "VlF",
    "VolumeMask": "VlM",
    "Fuse.Voronoi": "vor",
    "Vortex": "Vtx",
    "WandMask": "Wnd",
    "MtlWard": "3Wd",
    "Dimension.WarpColor": "WC",
    "Dimension.WarpDiff": "WarpDiff",
    "Dimension.WarpFlow": "WF",
    "Fuse.WaveModifier": "Wav",
    "Weld3D": "3We",
    "WhiteBalance": "WB",
    "Fuse.WirelessAnything": "Wrless",
    "Fuse.Wireless": "Wire",
    "Fuse.Wireless3D": "Wi3D",
    "Fuse.Wireless3DMAT": "Wi3DMAT",
    "Fuse.WordWriteOn": "WWO",
    "Fuse.XfChroma": "XfC",
    "Dimension.ZToDisparity": "Z2D",
    "ZtoWorldPos": "Z2W",
    "Fuse.ZcovertFuse": "Zconv",
}
# close UI on ESC button
comp.Execute(
    """app:AddConfig("Bookmarker",
{
Target  {ID = "Bookmarker"},
Hotkeys {Target = "Bookmarker",
         Defaults = true,
         ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}" }})
"""
)


def get_abbrev(tool_id):
    abbrev = regs_dictionary.get(tool_id)
    if abbrev:
        return ": ({})".format(abbrev)
    return ""


def parse_data(data):
    # return sorted by bookmark
    if fu.Version == 16.2:
        print(
            "Please upgrade Fusion. Download is available at https://support.blackmagicdesign.com"
        )
        return sorted(data.values(), key=lambda x: x[0].lower())
    parsed_data = sorted(
        [list(i.values()) for i in data.values()],
        key=lambda x: (regs_dictionary.get(x[4], ""), x[0].lower()),
    )
    return parsed_data


def prefill_combobox(data=None):
    itm["MyCombo"].Clear()
    if data is None or data == {}:
        message = "add some bookmarks!"
    else:
        message = "select bookmark"
    itm["MyCombo"].AddItem(message)
    itm["MyCombo"].InsertSeparator()


def fill_combobox(fill_data):
    prefill_combobox(fill_data)
    if fill_data:
        sorted_bms = [i[0] + get_abbrev(i[-1]) for i in parse_data(fill_data)]
        for bkm in sorted_bms:
            itm["MyCombo"].AddItem(bkm)


def delete_bookmark(key):
    global data
    comp = fu.GetCurrentComp()
    comp.SetData("BM.{}".format(key))
    del data[key]


def move_selected_to_bm(tools, target):
    comp = fu.GetCurrentComp()
    comp.StartUndo("Move tools to Bookmark")
    print("moving currently selected tools to Bookmarked position")
    pos_target_x, pos_target_y = flow.GetPosTable(target).values()
    for n, selected_tool in enumerate(tools):
        flow.SetPos(selected_tool, pos_target_x, pos_target_y + (n + 1))
    comp.EndUndo()


def set_previous(tool):
    comp.SetData("PrevBM", tool.Name)


def find_tool(tool_data, comp):
    bm_name, tool_name, scale_factor, tool_id_key = tool_data[:4]
    target = comp.FindTool(tool_name)
    if not target:
        print("looking for renamed tools")
        tool_dict = {}
        for tool in comp.GetToolList(False).values():
            tool_id = str(int(tool.GetAttrs()["TOOLI_ID"]))
            tool_dict[tool_id] = tool
        parse_key = str(tool_id_key[7:])
        tool_found = tool_dict.get(parse_key)
        if tool_found:
            print("found new name: {}".format(tool_found.Name))
            comp.SetData(
                "BM.{}".format(tool_id_key),
                [
                    bm_name,
                    tool_found.Name,
                    scale_factor,
                    tool_id_key,
                    tool_found.ID,
                ],
            )
            refresh_list()
            target = comp.FindTool(tool_found.Name)
    if target:
        return target, scale_factor
    else:
        print("No bookmarks found. Press clean up button to remove orphaned bookmarks")
        return None, None


def jump_bookmark(ev):
    choice = int(itm["MyCombo"].CurrentIndex) - 2
    comp = fu.GetCurrentComp()
    if choice >= 0 and data:
        tool_data = parse_data(data)[choice]
        target, scale_factor = find_tool(tool_data, comp)
        if not target:
            return
        tool_list = comp.GetToolList(True).values()
        flow.Select()
        flow.SetScale(scale_factor)
        time.sleep(0.1)
        comp.SetActiveTool(target)
        if tool_list:
            set_previous(list(tool_list)[0])
            if itm["moveCB"].Checked:
                move_selected_to_bm(tool_list, target)


def jump_back(ev):
    comp = fu.GetCurrentComp()
    previous_tool = comp.FindTool(comp.GetData("PrevBM"))
    current = comp.ActiveTool or previous_tool
    if not current:
        print("no previous tool to jump to")
        return
    if previous_tool:
        comp.SetActiveTool(previous_tool)
        comp.SetData("PrevBM", current.Name)


def close(ev):
    disp.ExitLoop()


def clear_all_bookmarks(ev):
    global data
    comp = fu.GetCurrentComp()
    comp.SetData("BM")
    data = {}
    print("all your bookmarks are belong to us")
    itm["MyCombo"].Clear()
    itm["MyCombo"].AddItem("add some bookmarks!")


def delete_bookmark_event(ev):
    choice = int(itm["MyCombo"].CurrentIndex)
    if choice > 0:
        bm_text, tool_id = parse_data(data)[choice - 2][::3]
        itm["MyCombo"].RemoveItem(choice)
        print("bookmark {} deleted".format(bm_text))
        delete_bookmark(tool_id)
        if len(data) == 0:
            prefill_combobox()
            print("no bookmarks left")
    else:
        print("cannot delete this bookmark")


def remove_orphaned(ev):
    comp = fu.GetCurrentComp()
    data = comp.GetData("BM")
    if not data:
        return
    tool_set = []
    for tool in comp.GetToolList(False).values():
        tool_set.append(tool.Name.lower())
    new_data = {k: v for k, v in data.items() if v[2].lower() in tool_set}
    if new_data != data:
        comp.SetData("BM")
        for k, v in new_data.items():
            comp.SetData("BM.{}".format(k), v)
        itm["MyCombo"].Clear()
        count = len(data) - len(new_data)
        suffix = ""
        if count > 1:
            suffix = "s"
        print("deleted {} orphaned bookmark{}".format(count, suffix))
        data = new_data
        if data == {}:
            prefill_combobox()
        else:
            fill_combobox(data)
    refresh_list()


def refresh_list(ev=None):
    comp = fu.GetCurrentComp()
    global data
    check_data = comp.GetData("BM")
    if not check_data:
        prefill_combobox()
        data = {}
        return
    if check_data and check_data != data:
        print("bookmarks list updated")
        itm["MyCombo"].Clear()
        data = check_data
        fill_combobox(data)
    else:
        print("nothing changed")


def add_bookmark_run(ev):
    comp.RunScript("Scripts:Comp/Bookmarker/bookmark_add.py")


def launch_ui(ui):
    # Main Window
    x, y = fu.GetMousePos().values()
    if y < 90:
        y = 120
    disp = bmd.UIDispatcher(ui)
    btn_icon_size = 0
    button_size = [65, 25]
    win = disp.AddWindow(
        {
            "ID": "Bookmarker",
            "TargetID": "Bookmarker",
            "WindowTitle": "jump to bookmark",
            "Geometry": [x, y, 330, 95],
        },
        [
            ui.VGroup(
                [
                    ui.HGroup(
                        [
                            ui.ComboBox(
                                {
                                    "ID": "MyCombo",
                                    "Text": "Choose preset",
                                    "Events": {"Activated": True},
                                    "Weight": 0.8,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "AddButton",
                                    "Flat": False,
                                    "IconSize": [12, 12],
                                    "MinimumSize": [20, 12],
                                    "Icon": ui.Icon(
                                        {
                                            "File": "Scripts:Comp/Bookmarker/icons/plus_icon.png"
                                        }
                                    ),
                                    "Weight": 0.1,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "refreshButton",
                                    "Flat": False,
                                    "IconSize": [12, 12],
                                    "MinimumSize": [20, 12],
                                    "Icon": ui.Icon(
                                        {
                                            "File": "Scripts:Comp/Bookmarker/icons/refresh_icon.png",
                                        }
                                    ),
                                    "Weight": 0.1,
                                }
                            ),
                        ]
                    ),
                    ui.HGroup(
                        [
                            ui.Button(
                                {
                                    "ID": "jb",
                                    "Text": "jump back",
                                    "Weight": 0.3,
                                    "MinimumSize": button_size,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "rm",
                                    "Text": "delete one",
                                    "Weight": 0.3,
                                    "MinimumSize": button_size,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "rmall",
                                    "Text": "reset all",
                                    "Weight": 0.3,
                                    "MinimumSize": button_size,
                                }
                            ),
                            ui.Button(
                                {
                                    "ID": "cleanupButton",
                                    "Text": "clean up",
                                    "Weight": 0.3,
                                    "MinimumSize": button_size,
                                }
                            ),
                        ]
                    ),
                    ui.HGroup(
                        [
                            ui.CheckBox(
                                {"ID": "moveCB", "Text": "move selected to bookmark?",}
                            )
                        ]
                    ),
                ]
            ),
        ],
    )

    itm = win.GetItems()
    return itm, win, disp

if __name__ == "__main__":
    data = comp.GetData("BM")

    if not data:
        data = {}
        print("add some bookmarks!")

    if data and fu.Version == 16.2:
        for k, v in data.items():
            if isinstance(v, dict):
                comp.SetData("BM.{}".format(k), list(v.values()))
        data = comp.GetData("BM")
    main_win = ui.FindWindow("Bookmarker")

    if main_win:
        main_win.Raise()
        main_win.ActivateWindow()
    else:
        itm, win, disp = launch_ui(ui)
        win.On.MyCombo.Activated = jump_bookmark
        win.On.rm.Clicked = delete_bookmark_event
        win.On.rmall.Clicked = clear_all_bookmarks
        win.On.jb.Clicked = jump_back
        win.On.Bookmarker.Close = close
        win.On.refreshButton.Clicked = refresh_list
        win.On.AddButton.Clicked = add_bookmark_run
        win.On.cleanupButton.Clicked = remove_orphaned
        try:
            fill_combobox(data)
        except AttributeError:
            print(
                "Something is terribly wrong with Bookmarks database."
                "\nDelete all bookmarks to proceed and report this issue"
            )
            # itm["MyCombo"].Clear()
            data = {}
            # prefill_combobox(data)
        win.Show()
        disp.RunLoop()
        win.Hide()
