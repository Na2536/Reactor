--[[--
Retimes with percentage frame by frame.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------


Changelog:
1.1, 2020-03-05:
* Added Retimer3D, retime your 3D scenes!
* Added an offset slider so you can start your shot somewhere else
* Cleaned up complicated code so the script now self-reference itself 

1.0.2, 2020-01-19:
* Change the Interpolation Mode's dropdown/button from ComboControl to MultiButtonControl to make it work better in Fusion 9

1.0.1, 2020-01-12:
* Change Interpolation Mode from dropdown list to multi button to work better in Fusion 9

1.0, 2020-01-09:
* Initial release

--]]--

FuRegisterClass("Retimer", CT_Tool, {
	REGS_Name = "Retimer",
	REGS_Category = "Miscellaneous",
	REGS_OpIconString = "rt",
	REGS_OpDescription = "Retimes with percentage frame by frame",
	
	REGS_Company = "Jacob Danell",
	REGS_URL = "http://www.emberlight.se",
	REG_Version = 110,

	REG_NoMotionBlurCtrls = true,
	REG_OpNoMask = false,
	REG_NoBlendCtrls = false,
	REG_NoObjMatCtrls = false,
	REG_NoPreCalcProcess = true,

	REG_TimeVariant = true,
})

FuRegisterClass("Retimer3D", CT_Tool, {
	REGS_Name = "Retimer3D",
	REGS_Category = "Miscellaneous",
	REGS_OpIconString = "rt3D",
	REGS_OpDescription = "Retimes with percentage frame by frame for 3D scenes",
	
	REGS_Company = "Jacob Danell",
	REGS_URL = "http://www.emberlight.se",
	REG_Version = 100,

	REG_NoMotionBlurCtrls = true,
	REG_OpNoMask = true,
	REG_NoBlendCtrls = true,
	REG_NoObjMatCtrls = true,
	REG_NoPreCalcProcess = true,

	REG_TimeVariant = true,
	REGB_Temporal 	= true,
})

function Create()

	ModType = ffi.string(self.RegNode.m_ID):sub(6) -- get the ID we were created with, and strip 'Fuse.'

	InTime = self:AddInput("Speed", "Speed", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default = 1,
		INP_MinAllowed = 0,
		INP_MaxScale = 10,
	})

	InOffset = self:AddInput("Offset", "Offset", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Default = 0,
		INP_Integer = true,
		INP_MinScale = 0,
		INP_MaxScale = 100,
	})

	if ModType == "Retimer" then

		InInterpolation = self:AddInput("Interpolation Mode", "InterpolationMode", {
			LINKID_DataType = "Number",
			INPID_InputControl = "MultiButtonControl",
			ICD_Width = 1,
			INP_Default = 0.0,
			INP_Integer = true,
			--MBTNC_ForceButtons = true,
			MBTNC_StretchToFit = true,
			{ MBTNC_AddButton = "Nearest", },
			{ MBTNC_AddButton = "Blend", },
			{ MBTNC_AddButton = "Subframe", },
		})
	
		InShowFrameInMetadata = self:AddInput("Show frame in metadata", "ShowFrameInMetadata", {
			LINKID_DataType = "Number",
			INPID_InputControl = "CheckboxControl",
			INP_MinAllowed =   0.0,
			INP_MaxAllowed =   1.0,
			INP_Default = 0.0,
		})

		InImage = self:AddInput("Input", "Input", {
			LINKID_DataType = "Image",
			LINK_Main = 1,
		})
	
		OutImage = self:AddOutput("Output", "Output", {
			LINKID_DataType = "Image",
			LINK_Main = 1,
		})

	else -- Retimer3D

		InInterpolation = self:AddInput("Interpolation Mode", "InterpolationMode", {
			LINKID_DataType = "Number",
			INPID_InputControl = "MultiButtonControl",
			ICD_Width = 1,
			INP_Default = 0.0,
			INP_Integer = true,
			--MBTNC_ForceButtons = true,
			MBTNC_StretchToFit = true,
			{ MBTNC_AddButton = "Nearest", },
			{ MBTNC_AddButton = "Subframe", },
		})

		InImage = self:AddInput("3D Input", "3DInput", {
			LINKID_DataType = "DataType3D",
			LINK_Main = 1,
			INP_SendRequest = false,
			INP_Required = false,
		})
	
		OutImage = self:AddOutput("Output", "Output", {
			LINKID_DataType = "DataType3D",
			LINK_Main = 1,
		})

	end
end

function Process(req)

	local globalstart = self.Comp.GlobalStart
	local offset = InOffset:GetValue(req).Value

	-- get tool inputs
	local frame = globalstart + offset
	local interpolation = InInterpolation:GetValue(req).Value
	local out = nil
	local showframeinmeta = 0

	-- If not 3D, create copy of org image
	if ModType == "Retimer" then
		showframeinmeta = InShowFrameInMetadata:GetValue(req).Value
		local img = InImage:GetValue(req)
	
		-- create output image
		local out = Image({
			IMG_Like       = img,
			IMG_NoData     = req:IsPreCalc(),
			IMG_DeferAlloc = true,
			IMG_NoData = req:IsPreCalc(),
		})
	end

	-- Calculate current frame to show
	local curTime = req.Time
	local modifier = 0
	for i=globalstart, curTime-1 do
		modifier = InTime:GetSource(i, REQF_SecondaryTime).Value
		frame = frame + (1*modifier)
	end

	if interpolation == 0 then -- Nearest

		frame = math.floor(frame + 0.499) -- add 0.499 to round up or down depending if you're above 0.5 or below
		out = InImage:GetSource(frame, req:GetFlags(), { RQA_Request = req })

	elseif ModType == "Retimer" and interpolation == 1 then -- Blend (if not 3D)

		local frame1 = math.floor(frame)
		local frame2 = math.floor(frame+1)
		local blendamount = frame - frame1

		local img1 = Image({
			IMG_Like       = img,
			IMG_NoData     = req:IsPreCalc(),
			IMG_DeferAlloc = true,
			IMG_NoData = req:IsPreCalc(),
		})
		img1 = InImage:GetSource(frame1, req:GetFlags(), { RQA_Request = req })

		local img2 = Image({
			IMG_Like       = img,
			IMG_NoData     = req:IsPreCalc(),
			IMG_DeferAlloc = true,
			IMG_NoData = req:IsPreCalc(),
		})
		img2 = InImage:GetSource(frame2, req:GetFlags(), { RQA_Request = req })

		out = img1:BlendOf(img2, blendamount)

	else -- Sub-frame

		out = InImage:GetSource(frame, req:GetFlags(), { RQA_Request = req })

	end

	if showframeinmeta == 1 then
		local newmetadata = out.Metadata or {}
    	newmetadata["Remap frame"] = frame
		out.Metadata = newmetadata
	end

	OutImage:Set(req, out)
	collectgarbage()
	
end